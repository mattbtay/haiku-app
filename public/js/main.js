
$(document).ready(function() {

    //make top section the size of the browser
    //$('#top').css('height', $(window).height());

	//photo uploader during testing
	$('#photoUploader button').click(function(){
		var myPhoto = $('#photoUpload').val();
		//alert(myPhoto);
		$('.poem-photo img').prop('src', myPhoto);
		$('#poemPhoto').val(myPhoto);
	});


    function showError(error) {
        var message = "An error occurred";
        if (error.message) {
                message = error.message;
        } else if (error.errors) {
                var errors = error.errors;
                message = "";
                Object.keys(errors).forEach(function(k) {
                        message += k + ": " + errors[k] + "\n";
                });
        }

        alert(message);
}

    function loadPoems() {
    dpd.poems.get(function(poems, error) { //Use dpd.js to access the API
        $('#stream').empty(); //Empty the list
        poems.forEach(function(poem) { //Loop through the result
            addPoem(poem); //Add it to the DOM.
        });
    });

    }

    loadPoems();

    $('#poem-form').submit(function() {
        //Get the data from the form
        var author = $('#author').val();
        var line1 = $('#line1').val();
        var line2 = $('#line2').val();
        var line3 = $('#line3').val();
        var poemPhoto = $('#poemPhoto').val();

        dpd.poems.post({
        line1: line1,
        line2: line2,
        line3: line3,
        author: author,
        poemPhoto: poemPhoto
        }, function(poems, error) {
        if (error) return showError(error);

        addPoem(poems);
            $('#line1').val('');
            $('#line2').val('');
            $('#line3').val('');
            $('#author').val('');
            $('#poemPhoto').val('');
        });

        return false;
    });

    function addPoem(poems) {
        $('<div class="item group">')
        	.append("<img src='" + poems.poemPhoto + "'/>")
            .append('<p class="poem-text">' + poems.line1 + '<br>' + poems.line2 + '<br>' + poems.line3  + '</p>' )
            .append('<p class="poem-author">by: ' + poems.author + '</p>')
            .appendTo('#stream');
    }



    // count characters in form fields
    $('.poem-line').keyup(function(){
    	//get count in the input field
    	var charCount = $(this).val().length;
    	//put count in counter box
    	$(this).prev().text(charCount);
    	// put limit on count
    	if (charCount > 50) {
    		$(this).prev().removeClass('valid').addClass('error');
    	} else if (charCount === 50) {
            $(this).prev().removeClass('error').addClass('valid');
        } else if (charCount < 50) {
            $(this).prev().removeClass('error').removeClass('valid');
        }
    	
    });













});